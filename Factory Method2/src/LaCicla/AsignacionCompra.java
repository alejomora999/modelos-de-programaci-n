package LaCicla;

public class AsignacionCompra implements ComprarBicicleta {

	@Override
	public Bicicleta adquirirBicicleta(String marco, String ruedas, String mango, String manubrio, String cadena,
			String pi�on, String frenos) {
		if(marco.toLowerCase().equals("fibra de carbono")&& ruedas.toLowerCase().equals("delgada")) {
			return new Carretera(marco, ruedas, mango, manubrio, cadena, pi�on,frenos);
			
		}else if(marco.toLowerCase().equals("aluminio")&& ruedas.toLowerCase().equals("mediana")) {
			return new Pista(marco, ruedas, mango, manubrio, cadena, pi�on,frenos);
			
		}else if(marco.toLowerCase().equals("acero")&& ruedas.toLowerCase().equals("gruesa")) {
			return new Bmx(marco, ruedas, mango, manubrio, cadena, pi�on,frenos);
		}
		return null;
	}
	

}
