package LaCicla;

public abstract class Bicicleta {
	private String marco = "";
	private String ruedas = "";
	private String mango = "";
	private String manubrio = "";
	private String cadena = "";
	private String pi�on = "";
	private String frenos = "";

	public Bicicleta(String marco, String ruedas, String mango, String manubrio, String cadena, String pi�on,
			String frenos) {
		setCadena(cadena);
		setFrenos(frenos);
		setMango(mango);
		setManubrio(manubrio);
		setPi�on(pi�on);
		setRuedas(ruedas);
		setMarco(marco);
	}

	public abstract String bicicleta();// Este es el metodo

	public String getMarco() {
		return marco;
	}

	public void setMarco(String marco) {
		this.marco = marco;
	}

	public String getRuedas() {
		return ruedas;
	}

	public void setRuedas(String ruedas) {
		this.ruedas = ruedas;
	}

	public String getMango() {
		return mango;
	}

	public void setMango(String mango) {
		this.mango = mango;
	}

	public String getManubrio() {
		return manubrio;
	}

	public void setManubrio(String manubrio) {
		this.manubrio = manubrio;
	}

	public String getCadena() {
		return cadena;
	}

	public void setCadena(String cadena) {
		this.cadena = cadena;
	}

	public String getPi�on() {
		return pi�on;
	}

	public void setPi�on(String pi�on) {
		this.pi�on = pi�on;
	}

	public String getFrenos() {
		return frenos;
	}

	public void setFrenos(String frenos) {
		this.frenos = frenos;
	}

}
