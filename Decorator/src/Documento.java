class Documento {
	public void operacion() {
		System.out.println("Creando documento");
	}

	public void close() {
		System.out.println("Cerrando documento");
	}
}

abstract class DocumentoCodificado extends Documento {
	private Documento documento;

	public DocumentoCodificado(Documento componente) {
		this.documento = componente;
	}

	public void operacion() {
		documento.operacion();
	}
}

class DecoradorConcretoA extends DocumentoCodificado {
	public DecoradorConcretoA(Documento componente) {
		super(componente);
	}

	public void operacion() {
		super.operacion();
		System.out.println("Separador al documento");
	}
}

class DecoradorConcretoB extends DocumentoCodificado {
	public DecoradorConcretoB(Documento componente) {
		super(componente);
	}

	public void operacion() {
		super.operacion();
		System.out.println("Contando lineas del documento");
	}
}