

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setTitle("Empresa X");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Contabilidad");
		rdbtnNewRadioButton.setBounds(17, 38, 109, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Gerencia");
		rdbtnNewRadioButton_1.setBounds(17, 64, 109, 23);
		contentPane.add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Produccion");
		rdbtnNewRadioButton_2.setBounds(17, 90, 109, 23);
		contentPane.add(rdbtnNewRadioButton_2);
		
		JRadioButton rdbtnNewRadioButton_3 = new JRadioButton("Administracion");
		rdbtnNewRadioButton_3.setBounds(17, 116, 109, 23);
		contentPane.add(rdbtnNewRadioButton_3);
		
		JRadioButton rdbtnNewRadioButton_4 = new JRadioButton("Ventas");
		rdbtnNewRadioButton_4.setBounds(17, 142, 109, 23);
		contentPane.add(rdbtnNewRadioButton_4);
		
		textField = new JTextField();
		textField.setBounds(300, 38, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(300, 64, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(300, 90, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblDepartamentosPresentes = new JLabel("Departamentos Presentes");
		lblDepartamentosPresentes.setBounds(10, 11, 153, 20);
		contentPane.add(lblDepartamentosPresentes);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(212, 41, 46, 14);
		contentPane.add(lblNombre);
		
		JLabel lblCedula = new JLabel("Cedula");
		lblCedula.setBounds(212, 67, 46, 14);
		contentPane.add(lblCedula);
		
		JLabel lblSueldo = new JLabel("Sueldo");
		lblSueldo.setBounds(212, 93, 46, 14);
		contentPane.add(lblSueldo);
		
		JButton btnAgregarEmpleado = new JButton("Agregar Empleado");
		btnAgregarEmpleado.setBounds(212, 142, 144, 23);
		contentPane.add(btnAgregarEmpleado);
		
		JButton btnConsultarSueldoDel = new JButton("Consultar Sueldo Y Cargo");
		btnConsultarSueldoDel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnConsultarSueldoDel.setBounds(212, 176, 174, 23);
		contentPane.add(btnConsultarSueldoDel);
		
		JButton btnNewButton = new JButton("Consultar Suedo Y Cargo");
		btnNewButton.setBounds(212, 210, 174, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblTodosLosEmpleados = new JLabel("Todos Los Empleados");
		lblTodosLosEmpleados.setBounds(97, 180, 105, 14);
		contentPane.add(lblTodosLosEmpleados);
		
		JLabel lblEmpleadoEspecifico = new JLabel("Empleado Especifico");
		lblEmpleadoEspecifico.setBounds(97, 214, 105, 14);
		contentPane.add(lblEmpleadoEspecifico);
	}
}
