package Francesa;

public class Baraja {
	private static Baraja instance;
	private String nombreBaraja;
	private Baraja(){ //constructor privado
		
	}
	
	public static Baraja getInstance(){ //Metodo que instancia la baraja
		if(instance==null){
			instance= new Baraja();
			
		}
		return instance;		
	}

	public String getNombreBaraja() {
		return nombreBaraja;
	}

	public void setNombreBaraja(String nombreBaraja) {
		this.nombreBaraja = nombreBaraja;
	}
	

}
