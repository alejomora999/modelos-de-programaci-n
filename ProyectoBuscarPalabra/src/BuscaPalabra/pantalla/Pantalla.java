package BuscaPalabra.pantalla;

import BuscaPalabra.io.Input;

/**
 *
 * @author ssalaza
 */
public class Pantalla {

    Input input = null;
    String textoCargado = "";

    public void pantalla() {
        input = new Input();
        System.out.println("Para digitar un texto digite la opcion 1 :");
        System.out.println("Para cargar un archivo digite la opcion 2 :");
        validaOpciones(input.inputSystem());
    }

    public void pantallaTexoABuscar(String textoCargado) {
        System.out.println("Digite la palabra a buscar en el texto");
        validaTexto(textoCargado, input.inputSystem());
    }

    public void validaOpciones(String opcion) {
        input = new Input();
        switch (opcion) {
            case "1":
                System.out.println("Digite o copie el texto a evaluar:");
                textoCargado = input.inputSystem();
                pantallaTexoABuscar(textoCargado);
                break;
            case "2":
                System.out.println("Digite la ruta del archivo: ");
                String path = input.inputSystem();
                if (!path.equals("") || path != null) {
                    textoCargado = input.inputFile(path);
                } else {
                    System.out.println("No se capturo un valor de ruta de archivo.");
                    pantalla();
                }
                break;
            case "exit":
                System.exit(0);
            default:
                System.out.println("Opcion invalida");
                pantalla();
                break;
        }
    }

    public void validaTexto(String textoCargado, String textoABuscar) {
        if (textoABuscar.equals("")) {
            System.out.println("No se registro ningun texto.");
            System.out.println("Se reiniciara el programa.....");
            System.out.println("");
            pantalla();
        } else if (textoCargado.contains(textoABuscar)) {
            int index = textoCargado.indexOf(textoABuscar);
            int contador = 0;
            while (contador != -1) {
                contador++;
                index = textoCargado.indexOf(textoABuscar, index + 1);
            }
            System.out.println("Palabra encontrada "+contador+" veces");
        } else {
        }
    }

}
