package BuscaPalabra.io;

import BuscaPalabra.pantalla.Pantalla;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ssalaza
 */
public class Input {
    
   Pantalla pantalla = null;

    public String inputSystem() {
        pantalla = new Pantalla();
        String inpuText = "";
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            inpuText = bufferedReader.readLine();
            
        } catch (IOException e) {
            System.out.println("Ocurrio un error en la entrada por consola " + e);
        }
        return inpuText;
    }

    public String inputFile(String path) {
        pantalla = new Pantalla();
        FileReader fileReader = null;
        String texto = "";
        try {
            File file = new File(path);
            fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            System.out.println("Se cargo exitosamente el archivo: "+file.getName());
            texto = bufferedReader.readLine();
            System.out.println("Este es: "+texto);
            return texto;
        } catch (FileNotFoundException ex) {
            System.out.println("No se encontro el archivo en la ruta especificada");
            pantalla.pantalla();
        } catch (IOException ex) {
            Logger.getLogger(Input.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileReader.close();
            } catch (IOException ex) {
                System.out.println(ex);
            }
        }

        return texto;
    }

}
