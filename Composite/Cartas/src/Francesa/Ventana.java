package Francesa;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import java.awt.Color;

public class Ventana extends JFrame {

	private JPanel contentPane;

	void crearEtiquetas(int atrapar,  int des1){ 
		JLabel etiqueta[] = new JLabel[atrapar];
        if(des1==500){
        	Treboles trebol= new Treboles();
        	int des=38;
            for(int i=2; i<atrapar; i++){ 
            	int atrapar1=(int) (Math.random() * 13) + 2;
            	System.out.println(atrapar1);
            	Pintas clonacion;
    			clonacion=trebol.clonar();//Clono caso original
    			clonacion.setValor(atrapar1);
                etiqueta[i]= new JLabel(); 
                etiqueta[i].setBounds(des1,des,82,110); 
                des+=50;
                getContentPane().add(etiqueta[i]);
                ImageIcon t= new ImageIcon(clonacion.getNombre());
    			ImageIcon icono1= new ImageIcon(t.getImage().getScaledInstance(etiqueta[i].getWidth(), etiqueta[i].getHeight(), Image.SCALE_DEFAULT));
    			etiqueta[i].setIcon(icono1);
            }
        }else if(des1==371){
        	Diamantes trebol= new Diamantes();
        	int des=38;
            for(int i=2; i<atrapar; i++){ 
            	int atrapar1=(int) (Math.random() * 13) + 2;
            	System.out.println(atrapar1);
            	Pintas clonacion;
    			clonacion=trebol.clonar();//Clono caso original
    			clonacion.setValor(atrapar1);
                etiqueta[i]= new JLabel(); 
                etiqueta[i].setBounds(des1,des,82,110); 
                des+=50;
                getContentPane().add(etiqueta[i]);
                ImageIcon t= new ImageIcon(clonacion.getNombre());
    			ImageIcon icono1= new ImageIcon(t.getImage().getScaledInstance(etiqueta[i].getWidth(), etiqueta[i].getHeight(), Image.SCALE_DEFAULT));
    			etiqueta[i].setIcon(icono1);
            }
        }else if(des1==207){
        	Picas trebol= new Picas();
        	int des=38;
            for(int i=2; i<atrapar; i++){ 
            	int atrapar1=(int) (Math.random() * 13) + 2;
            	System.out.println(atrapar1);
            	Pintas clonacion;
    			clonacion=trebol.clonar();//Clono caso original
    			clonacion.setValor(atrapar1);
                etiqueta[i]= new JLabel(); 
                etiqueta[i].setBounds(des1,des,82,110); 
                des+=50;
                getContentPane().add(etiqueta[i]);
                ImageIcon t= new ImageIcon(clonacion.getNombre());
    			ImageIcon icono1= new ImageIcon(t.getImage().getScaledInstance(etiqueta[i].getWidth(), etiqueta[i].getHeight(), Image.SCALE_DEFAULT));
    			etiqueta[i].setIcon(icono1);
            }
        }else{
        	Corazones trebol= new Corazones();
        	int des=38;
            for(int i=2; i<atrapar; i++){ 
            	int atrapar1=(int) (Math.random() * 13) + 2;
            	System.out.println(atrapar1);
            	Pintas clonacion;
    			clonacion=trebol.clonar();//Clono caso original
    			clonacion.setValor(atrapar1);
                etiqueta[i]= new JLabel(); 
                etiqueta[i].setBounds(des1,des,82,110); 
                des+=50;
                getContentPane().add(etiqueta[i]);
                ImageIcon t= new ImageIcon(clonacion.getNombre());
    			ImageIcon icono1= new ImageIcon(t.getImage().getScaledInstance(etiqueta[i].getWidth(), etiqueta[i].getHeight(), Image.SCALE_DEFAULT));
    			etiqueta[i].setIcon(icono1);
            }
        }
		
      }
     
	public Ventana() {
		setTitle("CARTAS");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Usuario\\workspace\\Cartas\\Diamantes\\100.jpg"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 750, 1060);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 204, 0));
		contentPane.setForeground(new Color(0, 255, 51));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JLabel lbl40 = new JLabel();
		lbl40.setBounds(500, 0, 82, 110);
		contentPane.add(lbl40);
		JLabel lbl27 = new JLabel();
		lbl27.setBounds(371, 0, 82, 110);
		contentPane.add(lbl27);
		JLabel lbl14 = new JLabel();
		lbl14.setBounds(207, 0, 82, 110);
		contentPane.add(lbl14);
		JLabel lbl1 = new JLabel();
		lbl1.setBounds(47, 0, 82, 110);
		contentPane.add(lbl1);
		
		JButton btnC = new JButton();
		btnC.setText("CORAZONES");
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
					Corazones trebol= new Corazones();
					trebol.setValor(1);
					System.out.println(trebol.getNombre());
					ImageIcon t1= new ImageIcon(trebol.getNombre());
					ImageIcon icono= new ImageIcon(t1.getImage().getScaledInstance(lbl1.getWidth(), lbl1.getHeight(), Image.SCALE_DEFAULT));
					lbl1.setIcon(icono);
					int atrapar=(int) (Math.random() * 13) + 1;
					crearEtiquetas(atrapar, 47);
				
				
			}
		});
		btnC.setBounds(585, 246, 116, 23);
		contentPane.add(btnC);
		
		JButton btnP = new JButton();
		btnP.setText("PICAS");
		btnP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Picas trebol= new Picas();
				trebol.setValor(1);
				System.out.println(trebol.getNombre());
				ImageIcon t1= new ImageIcon(trebol.getNombre());
				ImageIcon icono= new ImageIcon(t1.getImage().getScaledInstance(lbl14.getWidth(), lbl14.getHeight(), Image.SCALE_DEFAULT));
				lbl14.setIcon(icono);
				int atrapar=(int) (Math.random() * 13) + 1;
				crearEtiquetas(atrapar, 207);
			}
		});
		btnP.setBounds(585, 324, 116, 23);
		contentPane.add(btnP);
		
		JButton btnD = new JButton();
		btnD.setText("DIAMANTES");
		btnD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Diamantes trebol= new Diamantes();
				trebol.setValor(1);
				System.out.println(trebol.getNombre());
				ImageIcon t1= new ImageIcon(trebol.getNombre());
				ImageIcon icono= new ImageIcon(t1.getImage().getScaledInstance(lbl27.getWidth(), lbl27.getHeight(), Image.SCALE_DEFAULT));
				lbl27.setIcon(icono);
				int atrapar=(int) (Math.random() * 13) + 1;
				crearEtiquetas(atrapar, 371);
			}
		});
		btnD.setBounds(585, 402, 116, 23);
		contentPane.add(btnD);
		
		JButton btnT = new JButton();
		btnT.setText("TREBOLES");
		btnT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
					
					Treboles trebol= new Treboles();
					trebol.setValor(1);
					System.out.println(trebol.getNombre());
					ImageIcon t1= new ImageIcon(trebol.getNombre());
					ImageIcon icono= new ImageIcon(t1.getImage().getScaledInstance(lbl40.getWidth(), lbl40.getHeight(), Image.SCALE_DEFAULT));
					lbl40.setIcon(icono);
					int atrapar=(int) (Math.random() * 13) + 1;
					crearEtiquetas(atrapar, 500);
				
			}
		});
		btnT.setBounds(585, 479, 116, 23);
		contentPane.add(btnT);
		
		JButton btnNewButton = new JButton("SALIR");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton.setBounds(585, 137, 116, 23);
		contentPane.add(btnNewButton);
	}
}
