package Francesa;

public interface Pintas {
	//Baraja crearBaraja= Baraja.getInstance();
	public void setNombre(String nom);
	public String getNombre();
	public void setValor(int valor);
	public int getValor();
	public Pintas clonar();

}
