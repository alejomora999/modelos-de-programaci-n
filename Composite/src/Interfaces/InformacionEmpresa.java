package Interfaces;

import java.util.ArrayList;

public interface InformacionEmpresa {
	public double getSueldo();

	public String getsectores();

	// public String getInfo(String cedula, int a, String nombre);
	// public String getCedula(String cedula);
	// public String getNombre(String nombre);
	// //public InformacionEmpresa clonar(String cedula, String nombre, String
	// cargo, double sueldo);
	public String getInfo(String cedula);

	public String getCedula();

	public String getCargo();

	public String getNombre();

	public void agregar(InformacionEmpresa p);

	public ArrayList<String> getCedulas();

	public ArrayList<String> getCargos();

	public ArrayList<String> getNombres();

	public ArrayList<String> getInfos();

	public String getInfo();

	public ArrayList<String> getSueldos();
}
