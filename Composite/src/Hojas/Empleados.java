package Hojas;

import java.util.ArrayList;
import Interfaces.InformacionEmpresa;

public class Empleados implements InformacionEmpresa {

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	private String nombre = "";
	private String cargo = "";
	private double sueldo = 0;
	private String cedula = "";

	public Empleados(String cedula, String nombre, String cargo, double sueldo) {
		setCargo(cargo);
		setNombre(nombre);
		setSueldo(sueldo);
		setCedula(cedula);
	}

	@Override
	public double getSueldo() {
		// TODO Auto-generated method stub
		return sueldo;
	}

	@Override
	public String getsectores() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> getInfos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCedula() {
		// TODO Auto-generated method stub
		return cedula;
	}

	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return nombre;
	}

	@Override
	public void agregar(InformacionEmpresa p) {
		// TODO Auto-generated method stub

	}

	@Override
	public ArrayList<String> getCedulas() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> getCargos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> getNombres() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInfo(String cedula) {
		// TODO Auto-generated method stub
		return getNombre() + " con cargo " + getCargo() + " con un sueldo de " + String.valueOf(getSueldo());
	}

	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return getInfo(cedula);
	}

	@Override
	public ArrayList<String> getSueldos() {
		return null;
	}

}
