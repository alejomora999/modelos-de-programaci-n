package Cliente;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Scanner;
import Compuestos.Administraccion;
import Compuestos.Contabilidad;
import Compuestos.EmpresaX;
import Compuestos.Gerencia;
import Compuestos.Produccion;
import Compuestos.Ventas;
import Hojas.Empleados;
import Interfaces.InformacionEmpresa;

public class Launcher {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaDatos frame = new VentanaDatos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
