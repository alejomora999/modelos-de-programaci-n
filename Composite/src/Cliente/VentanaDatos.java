package Cliente;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import Compuestos.Administraccion;
import Compuestos.Contabilidad;
import Compuestos.EmpresaX;
import Compuestos.Gerencia;
import Compuestos.Produccion;
import Compuestos.Ventas;
import Interfaces.InformacionEmpresa;

public class VentanaDatos extends JFrame {

	private EmpresaX empresita;
	private JPanel contentPane;
	private JTable table;
	public InformacionEmpresa[] sectorcito = { new Administraccion(), new Gerencia(), new Ventas(), new Produccion(),
			new Contabilidad() };
	private Ventana ventanita;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public VentanaDatos() {
		empresita = new EmpresaX();

		for (int i = 0; i < sectorcito.length; i++) {
			empresita.agregar(sectorcito[i]);
		}

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 721, 521);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(10, 11, 685, 50);
		contentPane.add(panel);
		panel.setLayout(null);

		String[] sect = { "Administraci�n", "Gerencia", "Ventas", "Producci�n", "Contabilidad", "Todos" };

		JRadioButton[] sectores = new JRadioButton[6];
		ButtonGroup group = new ButtonGroup();

		JLabel lblRegistros = new JLabel();
		lblRegistros.setForeground(Color.BLUE);
		lblRegistros.setFont(new Font("Tahoma", Font.ITALIC, 12));
		lblRegistros.setBounds(10, 450, 245, 21);

		JLabel lblSueldoTotal = new JLabel();
		lblSueldoTotal.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblSueldoTotal.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSueldoTotal.setHorizontalAlignment(SwingConstants.CENTER);
		lblSueldoTotal.setBounds(313, 424, 195, 32);

		for (int i = 0; i < sectores.length - 1; i++) {
			int j = i;
			sectores[i] = new JRadioButton(sect[i]);
			sectores[i].setBounds(6 + (111 * i), 7, 109, 23);
			sectores[i].addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					agregarDatos(empresita.getDatos().get(j), lblRegistros, lblSueldoTotal);
				}
			});
			panel.add(sectores[i]);
			group.add(sectores[i]);
		}

		sectores[5] = new JRadioButton(sect[5]);
		sectores[5].setBounds(6 + (111 * 5), 7, 109, 23);
		sectores[5].addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				agregarDatos(empresita.getDatos(), lblRegistros, lblSueldoTotal);
			}
		});
		panel.add(sectores[5]);
		group.add(sectores[5]);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 77, 498, 333);
		contentPane.add(scrollPane);

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null }, },
				new String[] { "C\u00E9dula", "Nombre", "Cargo", "Sueldo" }));
		scrollPane.setViewportView(table);

		contentPane.add(lblSueldoTotal);
		contentPane.add(lblRegistros);

		ventanita = new Ventana(this);

		JButton btnAgregarRegistro = new JButton("Agregar Registro");
		btnAgregarRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanita.setVisible(true);
				setVisible(false);
			}
		});
		btnAgregarRegistro.setBounds(545, 176, 133, 32);
		contentPane.add(btnAgregarRegistro);

		JButton btnVerRegistro = new JButton("Ver Registro");
		btnVerRegistro.setBounds(545, 238, 133, 32);
		btnVerRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,
						empresita.getInfo(JOptionPane.showInputDialog(null, "Ingrese n�mero de C�dula")));
			}
		});
		contentPane.add(btnVerRegistro);
	}

	public void agregarDatos(InformacionEmpresa empresita, JLabel registro, JLabel sueldoTotal) {
		DefaultTableModel modelo;

		ArrayList<Object[]> mod = new ArrayList<Object[]>();
		if (empresita.getCargos() == null) {
			mod.add(new Object[] { empresita.getCedula(), empresita.getNombre(), empresita.getCargo(),
					empresita.getSueldo() });
		}

		else {
			for (int j = 0; j < empresita.getCargos().size(); j++) {
				mod.add(new Object[] { empresita.getCedulas().get(j), empresita.getNombres().get(j),
						empresita.getCargos().get(j), empresita.getSueldos().get(j) });
			}
		}

		Object[][] modelin = new Object[mod.size()][4];

		for (int i = 0; i < modelin.length; i++) {
			modelin[i][0] = mod.get(i)[0];
			modelin[i][1] = mod.get(i)[1];
			modelin[i][2] = mod.get(i)[2];
			modelin[i][3] = mod.get(i)[3];
		}

		modelo = new DefaultTableModel(modelin, new String[] { "C\u00E9dula", "Nombre", "Cargo", "Sueldo" });
		table.setModel(modelo);
		sueldoTotal.setText("Sueldo Total: " + empresita.getSueldo());
		registro.setText("Cantidad de Registros: " + modelin.length);
	}

	public void agregarDatos(ArrayList<InformacionEmpresa> empresita, JLabel registro, JLabel sueldoTotal) {
		DefaultTableModel modelo;

		ArrayList<Object[]> mod = new ArrayList<Object[]>();
		for (int i = 0; i < empresita.size(); i++) {
			if (empresita.get(i).getCargos() == null) {
				mod.add(new Object[] { empresita.get(i).getCedula(), empresita.get(i).getNombre(),
						empresita.get(i).getCargo(), empresita.get(i).getSueldo() });
			} else {
				for (int j = 0; j < empresita.get(i).getCargos().size(); j++) {
					mod.add(new Object[] { empresita.get(i).getCedulas().get(j), empresita.get(i).getNombres().get(j),
							empresita.get(i).getCargos().get(j), empresita.get(i).getSueldos().get(j) });
				}
			}
		}

		Object[][] modelin = new Object[mod.size()][4];

		for (int i = 0; i < modelin.length; i++) {
			modelin[i][0] = mod.get(i)[0];
			modelin[i][1] = mod.get(i)[1];
			modelin[i][2] = mod.get(i)[2];
			modelin[i][3] = mod.get(i)[3];
		}

		modelo = new DefaultTableModel(modelin, new String[] { "C\u00E9dula", "Nombre", "Cargo", "Sueldo" });
		table.setModel(modelo);
		double temp = 0;
		for (int i = 0; i < modelin.length; i++) {
			temp += Double.parseDouble(modelin[i][3].toString());
		}
		sueldoTotal.setText("Sueldo Total: " + temp);
		registro.setText("Cantidad de Registros: " + modelin.length);
	}
}
