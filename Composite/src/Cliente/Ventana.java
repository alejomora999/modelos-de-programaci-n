package Cliente;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Hojas.Empleados;

import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtCedula;
	private JTextField txtSueldo;
	private JTextField txtCargo;
	private VentanaDatos ventanota;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public Ventana(VentanaDatos ventanota) {
		this.ventanota = ventanota;
		setTitle("Empresa");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setVisible(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		String[] sect = { "Administración", "Gerencia", "Ventas", "Producción", "Contabilidad" };
		JRadioButton[] sectores = new JRadioButton[5];
		ButtonGroup group=new ButtonGroup();
		
		for (int i = 0; i < sectores.length; i++) {
			sectores[i] = new JRadioButton(sect[i]);
			sectores[i].setBounds(19, 37 + (26 * i), 109, 23);
			contentPane.add(sectores[i]);
			group.add(sectores[i]);
		}

		sectores[0].setSelected(true);
		
		JLabel lblEmpleados = new JLabel("Empleados");
		lblEmpleados.setBounds(19, 16, 87, 14);
		contentPane.add(lblEmpleados);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(221, 41, 46, 14);
		contentPane.add(lblNombre);

		JLabel lblCedula = new JLabel("Cedula");
		lblCedula.setBounds(221, 67, 46, 14);
		contentPane.add(lblCedula);

		JLabel lblNewLabel = new JLabel("Sueldo");
		lblNewLabel.setBounds(221, 93, 46, 14);
		contentPane.add(lblNewLabel);

		txtNombre = new JTextField();
		txtNombre.setBounds(277, 38, 86, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);

		txtCedula = new JTextField();
		txtCedula.setBounds(277, 64, 86, 20);
		contentPane.add(txtCedula);
		txtCedula.setColumns(10);

		txtSueldo = new JTextField();
		txtSueldo.setBounds(277, 90, 86, 20);
		contentPane.add(txtSueldo);
		txtSueldo.setColumns(10);

		JLabel lblCargo = new JLabel("Cargo");
		lblCargo.setBounds(221, 117, 46, 14);
		contentPane.add(lblCargo);

		txtCargo = new JTextField();
		txtCargo.setColumns(10);
		txtCargo.setBounds(277, 114, 86, 20);
		contentPane.add(txtCargo);

		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(118, 192, 89, 23);
		btnAceptar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				for (int j = 0; j < sectores.length; j++) {
					if (sectores[j].isSelected()) {
						ventanota.sectorcito[j].agregar(new Empleados(txtCedula.getText(), txtNombre.getText(), txtCargo.getText(), Double.parseDouble(txtSueldo.getText())));
						txtCargo.setText("");
						txtCedula.setText("");
						txtNombre.setText("");
						txtSueldo.setText("");
						sectores[j].setSelected(false);
						j=sectores.length;
					}
				}
				ventanota.setVisible(true);
				dispose();
			}
		});
		contentPane.add(btnAceptar);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(240, 192, 89, 23);
		btnCancelar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ventanota.setVisible(true);
				dispose();
			}
		});

		contentPane.add(btnCancelar);
	}
}
