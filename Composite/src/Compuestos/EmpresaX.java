package Compuestos;

import java.util.ArrayList;
import Hojas.Empleados;
import Interfaces.InformacionEmpresa;

public class EmpresaX implements InformacionEmpresa {
	private ArrayList<InformacionEmpresa> empleados = new ArrayList<InformacionEmpresa>();

	@Override
	public double getSueldo() {
		// TODO Auto-generated method stub
		int suma = 0;
		for (int i = 0; i < empleados.size(); i++) {
			suma += empleados.get(i).getSueldo();
		}
		return suma;
	}

	public ArrayList<String> getSueldos() {
		ArrayList<String> temp = new ArrayList<String>();

		for (int i = 0; i < empleados.size(); i++) {
			if (empleados.get(i).getCargos() == (null)) {
				temp.add(String.valueOf(empleados.get(i).getSueldo()));
			} else {
				temp.addAll(empleados.get(i).getSueldos());
			}
		}
		return temp;
	}

	@Override
	public String getsectores() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInfo(String cedula) {
		for (int i = 0; i < empleados.size(); i++) {
			if (empleados.get(i).getCedulas() == null) {
				if (cedula.equals(empleados.get(i).getCedula())) {
					return empleados.get(i).getInfo(cedula);
				}
			} else {
				if (empleados.get(i).getCedulas().contains(cedula)) {
					return empleados.get(i).getInfo(cedula);
				}
			}
		}

		return null;
	}

	@Override
	public String getCedula() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCargo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregar(InformacionEmpresa p) {
		// TODO Auto-generated method stub
		empleados.add(p);

	}

	@Override
	public ArrayList<String> getCedulas() {
		// TODO Auto-generated method stub
		ArrayList<String> temp = new ArrayList<String>();

		for (int i = 0; i < empleados.size(); i++) {
			if (empleados.get(i).getCedulas() == (null)) {
				temp.add(empleados.get(i).getCedula());
			} else {
				// return empleados.get(i).getInfos();
				// for (int j=0; j<empleados.get(i).getInfos().size();j++){
				temp.addAll(empleados.get(i).getCedulas());
				// }

			}
		}
		return temp;
	}

	@Override
	public ArrayList<String> getCargos() {
		ArrayList<String> temp = new ArrayList<String>();

		for (int i = 0; i < empleados.size(); i++) {
			if (empleados.get(i).getCargos() == (null)) {
				temp.add(empleados.get(i).getCargo());
			} else {
				// return empleados.get(i).getInfos();
				// for (int j=0; j<empleados.get(i).getInfos().size();j++){
				temp.addAll(empleados.get(i).getCargos());
				// }

			}
		}
		return temp;
	}

	@Override
	public ArrayList<String> getNombres() {
		ArrayList<String> temp = new ArrayList<String>();

		for (int i = 0; i < empleados.size(); i++) {
			if (empleados.get(i).getNombres() == (null)) {
				temp.add(empleados.get(i).getNombre());
			} else {
				// return empleados.get(i).getInfos();
				// for (int j=0; j<empleados.get(i).getInfos().size();j++){
				temp.addAll(empleados.get(i).getNombres());
				// }

			}
		}
		return temp;
	}

	@Override
	public ArrayList<String> getInfos() {
		// TODO Auto-generated method stub

		ArrayList<String> temp = new ArrayList<String>();

		for (int i = 0; i < empleados.size(); i++) {
			if (empleados.get(i).getInfos() == (null)) {
				temp.add(empleados.get(i).getInfo());

			} else {
				// return empleados.get(i).getInfos();
				// for (int j=0; j<empleados.get(i).getInfos().size();j++){
				temp.addAll(empleados.get(i).getInfos());
				// }

			}
		}
		return temp;
	}

	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	public ArrayList<InformacionEmpresa> getDatos() {
		return this.empleados;
	}
}
