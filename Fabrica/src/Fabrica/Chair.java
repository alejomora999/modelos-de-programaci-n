package Fabrica;

public abstract class Chair {
	protected String espaldar;
	protected String material;
	public Chair (String espaldar, String material) {
		setEspaldar(espaldar);
		setMaterial(material);
	}
	
	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getEspaldar() {
		return espaldar;
	}

	public void setEspaldar(String espaldar) {
		this.espaldar = espaldar;
	}
	
	
}
