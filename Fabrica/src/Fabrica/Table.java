package Fabrica;

public abstract class Table {
	protected String altura;
	protected String material;
	
	public Table(String altura, String material) {
		setAltura(altura);
		setMaterial(material);
	}
	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	} 
	
}
