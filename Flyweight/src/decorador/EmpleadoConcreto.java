package decorador;

import java.awt.Color;

public class EmpleadoConcreto extends Empleado {
	private String nombreCompania;
	private String direccionCompania;
	private String cargo;
	private Color color;

	public String getCargo() {
		return cargo;
	}

	public Color getColor() {
		return color;
	}

	public String getNombreCompania() {
		return nombreCompania;
	}

	public String getDireccionCompania() {
		return direccionCompania;
	}

	public EmpleadoConcreto(String nombreCompania, String direccionCompania, String cargo, Color color) {
		this.nombreCompania = nombreCompania;
		this.direccionCompania = direccionCompania;
		this.cargo = cargo;
		this.color = color;
	}

	@Override
	public String getInfo() {
		return "Su cargo es " + getCargo() + " trabaja en " + getNombreCompania() + " que queda en "
				+ getDireccionCompania() + "\n";
	}

}
