package decorador;

public class EmpleadoDatos extends Empleado {
	private String nombre;
	private String apellido;

	private EmpleadoConcreto empleadito;

	public EmpleadoDatos(String nombre, String apellido, EmpleadoConcreto empleadito) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.empleadito = empleadito;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public EmpleadoConcreto getEmpleadito() {
		return empleadito;
	}

	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return "El nombre completo del empleado es " + getNombre() + " " + getApellido() + "\n" + empleadito.getInfo();
	}

}
