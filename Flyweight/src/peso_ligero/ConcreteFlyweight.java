package peso_ligero;

import decorador.EmpleadoConcreto;
import decorador.EmpleadoDatos;

public class ConcreteFlyweight extends Flyweight {

	public ConcreteFlyweight(EmpleadoConcreto empleadote) {
		setEmpleado(empleadote);
	}

	/*
	 * Este m�todo queda vac�o ya que en esta clase solo se crea el estado
	 * Intr�nseco
	 */
	@Override

	public EmpleadoDatos agregarInfo(String nombre, String apellido) {
		UnsharedFlyweight ligerin = new UnsharedFlyweight(getEmpleado());
		return ligerin.agregarInfo(nombre, apellido);
	}

}
