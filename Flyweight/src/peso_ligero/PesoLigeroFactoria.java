package peso_ligero;

import java.util.ArrayList;

import decorador.EmpleadoDatos;

public class PesoLigeroFactoria {
	public ArrayList<Flyweight> datos = new ArrayList<Flyweight>();

	public Flyweight getFlyweight(EmpleadoDatos concretin) {
		int i = 0;
		Flyweight fly = null;
		boolean bandera = false;
		if (datos.size() > 0) {
			for (i = 0; i < datos.size() && !bandera; i++) {
				if (datos.get(i).getEmpleado().getCargo() == concretin.getEmpleadito().getCargo()) {
					bandera = datos.get(i).getEmpleado().getCargo() == concretin.getEmpleadito().getCargo();
					fly = datos.get(i);
				}
			}
			if (bandera) {
				return fly;
			} else {
				datos.add(new ConcreteFlyweight(concretin.getEmpleadito()));
				return datos.get(datos.size() - 1);
			}

		} else {
			datos.add(new ConcreteFlyweight(concretin.getEmpleadito()));
			return datos.get(0);
		}
	}

}
