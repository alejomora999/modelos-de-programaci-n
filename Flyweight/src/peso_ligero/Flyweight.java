package peso_ligero;

import decorador.EmpleadoConcreto;
import decorador.EmpleadoDatos;

public abstract class Flyweight {
	private EmpleadoConcreto empleadito;

	public void setEmpleado(EmpleadoConcreto empleado) {
		empleadito = empleado;
	}

	public EmpleadoConcreto getEmpleado() {
		// TODO Auto-generated method stub
		return empleadito;
	}

	public abstract EmpleadoDatos agregarInfo(String nombre, String apellido);
}
