package peso_ligero;

import decorador.EmpleadoConcreto;
import decorador.EmpleadoDatos;

public class UnsharedFlyweight extends Flyweight {
	private EmpleadoDatos empleado;

	public String getInformacion() {
		return empleado.getInfo();
	}

	public UnsharedFlyweight(EmpleadoConcreto empleado) {
		setEmpleado(empleado);
	}

	@Override
	public EmpleadoDatos agregarInfo(String nombre, String apellido) {
		empleado = new EmpleadoDatos(nombre, apellido, super.getEmpleado());
		return empleado;
	}

}
