package cliente;

import java.awt.Color;
import java.util.ArrayList;

import decorador.Empleado;
import decorador.EmpleadoConcreto;
import decorador.EmpleadoDatos;
import peso_ligero.Flyweight;
import peso_ligero.PesoLigeroFactoria;

public class Main {

	public static void main(String[] args) {
		String nombreCompania = " Aerolineas TuGfa ";
		String direccionCompania = " Tv. 78 # 95 - 34b ";
		ArrayList<EmpleadoDatos> empleados = new ArrayList<EmpleadoDatos>();
		EmpleadoConcreto[] plantilla = {
				new EmpleadoConcreto(nombreCompania, direccionCompania, " Ingeniero ", new Color(255, 0, 0)),
				new EmpleadoConcreto(nombreCompania, direccionCompania, " Secretario ", new Color(0, 255, 0)),
				new EmpleadoConcreto(nombreCompania, direccionCompania, " Programador ", new Color(0, 0, 255)) };

		PesoLigeroFactoria fabrica = new PesoLigeroFactoria();
		Flyweight fly;
		int numeroFactoria = 0;

		empleados.add(new EmpleadoDatos(" Edwin Aaron ", " Garcia Pulido ", plantilla[0]));
		empleados.add(new EmpleadoDatos(" Alejandro ", " Morales Mojica ", plantilla[0]));
		empleados.add(new EmpleadoDatos(" Nathaly  ", " Valencia Ni�o ", plantilla[1]));
		empleados.add(new EmpleadoDatos(" Juan  ", " Pablo Perea ", plantilla[1]));
		empleados.add(new EmpleadoDatos(" Paula ", " Andrea Gomez ", plantilla[2]));
		empleados.add(new EmpleadoDatos(" Nicolas ", " Meneses Guerrero ", plantilla[2]));
		for (int i = 0; i < empleados.size(); i++) {
			fly = fabrica.getFlyweight(empleados.get(i));
		}
		numeroFactoria = fabrica.datos.size();

		Ventana ventana = new Ventana(fabrica, empleados);
		ventana.setVisible(true);
	}
}
