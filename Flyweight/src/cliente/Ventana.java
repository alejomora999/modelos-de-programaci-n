package cliente;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import decorador.*;
import peso_ligero.*;

public class Ventana extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField textField;
	private PesoLigeroFactoria fabrica;
	private ArrayList<EmpleadoDatos> datosCompletos;
	private JTextArea textArea;
	/**
	 * Launch the application.
	 */
	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// Ventana frame = new Ventana();
	// frame.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	/**
	 * Create the frame.
	 */
	public Ventana(PesoLigeroFactoria fabrica, ArrayList<EmpleadoDatos> datosCompletos) {
		this.fabrica = fabrica;
		this.datosCompletos = datosCompletos;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 668, 324);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 632, 263);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblEmpresa = new JLabel(fabrica.datos.get(0).getEmpleado().getNombreCompania());
		lblEmpresa.setBounds(89, 11, 394, 25);
		lblEmpresa.setFont(new Font("Yu Gothic UI Light", Font.ITALIC, 15));
		lblEmpresa.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblEmpresa);

		textField = new JTextField();
		textField.setBorder(new LineBorder(Color.BLACK));
		textField.setBackground(Color.LIGHT_GRAY);
		textField.setBounds(249, 47, 86, 20);
		panel.add(textField);
		textField.setColumns(10);

		JLabel lblIdEmpleado = new JLabel("Id. Empleado: ");
		lblIdEmpleado.setBounds(147, 50, 92, 14);
		lblIdEmpleado.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(lblIdEmpleado);

		JLabel lblNewLabel = new JLabel("Max. Datos " + datosCompletos.size());
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.ITALIC, 9));
		lblNewLabel.setBounds(259, 74, 86, 14);
		panel.add(lblNewLabel);

		textArea = new JTextArea();
		textArea.setBorder(new LineBorder(new Color(0, 0, 0)));
		textArea.setLineWrap(true);
		textArea.setEditable(false);
		textArea.setBackground(new Color(240, 240, 240));
		textArea.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		textArea.setBounds(10, 118, 612, 134);
		panel.add(textArea);

		JButton btnMostrar = new JButton("Mostrar");
		btnMostrar.setForeground(Color.WHITE);
		btnMostrar.setBackground(Color.LIGHT_GRAY);
		btnMostrar.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnMostrar.setBounds(345, 46, 69, 23);
		btnMostrar.addActionListener(this);
		panel.add(btnMostrar);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		int indicador = Integer.parseInt(textField.getText()) - 1;
		if (indicador < datosCompletos.size() && indicador >= 0) {
			EmpleadoDatos empleado = fabrica.getFlyweight(datosCompletos.get(indicador)).agregarInfo(
					datosCompletos.get(indicador).getNombre(), datosCompletos.get(indicador).getApellido());
			textArea.setText(empleado.getInfo());
			textArea.setForeground(empleado.getEmpleadito().getColor());
		}
	}

}
