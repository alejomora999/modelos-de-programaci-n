package Ejercicio1;

public class BMXBuilder extends BicicletaBuilder {

	@Override
	public void buildMarco(String marco) {
		bicicleta.setMarco(marco.toLowerCase());

	}

	@Override
	public void builRuedas(String ruedas) {
		bicicleta.setRuedas(ruedas.toLowerCase());

	}

	@Override
	public void buildMango() {
		bicicleta.setMango("espuma");

	}

	@Override
	public void buildManubrio() {
		bicicleta.setManubrio("acero");

	}

	@Override
	public void buildcadena() {
		bicicleta.setCadena("1 metro de largo");
	}

	@Override
	public void buildpi�on() {
		bicicleta.setPi�on("�nico");
	}

	@Override
	public void buildfrenos() {
		bicicleta.setFrenos("pinzas");
	}

}
