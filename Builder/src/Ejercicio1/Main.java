package Ejercicio1;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		System.out.println(
				"BIENVENIDO  USTED COMPRARA UNA BICILETA\n ESCRIBA EL MARCO  (FIBRA DE CARBONO, ALUMINIO O ACERO)");
		Scanner leer = new Scanner(System.in);
		Scanner leer1 = new Scanner(System.in);
		String marco = leer.nextLine();
		System.out.println("ESCRIBA EL TIPO DE RUEDA (DELGADA, MEDIANA O GRUESA)");
		String rueda = leer1.nextLine();
		Ensamblaje montar = new Ensamblaje();
		montar.setMarco(marco);
		montar.setRuedas(rueda);
		if (montar.getMarco().toLowerCase().equals("fibra de carbono")) {
			montar.setBicicletaBuilder(new CarreteraBuilder());
			System.out.println("BICICLETA CARRETERA CONSTRUIDA");

		} else if (montar.getMarco().toLowerCase().equals("aluminio")) {
			montar.setBicicletaBuilder(new PistaBuilder());
			System.out.println("BICICLETA PISTA CONSTRUIDA");

		} else if (montar.getMarco().toLowerCase().equals("acero")) {
			montar.setBicicletaBuilder(new BMXBuilder());
			System.out.println("BICICLETA BMX CONSTRUIDA");
		} else {

		}
		montar.montarBicicleta();
		Bicicleta bicicleta = montar.getBicileta();
		System.out.println("marco: " + bicicleta.getMarco());
		System.out.println("ruedas: " + bicicleta.getRuedas());
		System.out.println("manubrio: " + bicicleta.getManubrio());
		System.out.println("mango: " + bicicleta.getMango());
		System.out.println("cadena: " + bicicleta.getCadena());
		System.out.println("pi�on: " + bicicleta.getPi�on());
		System.out.println("frenos: " + bicicleta.getFrenos());

	}

}
