package Ejercicio1;

public class PistaBuilder extends BicicletaBuilder {

	@Override
	public void buildMarco(String marco) {
		bicicleta.setMarco(marco.toLowerCase());

	}

	@Override
	public void builRuedas(String ruedas) {
		bicicleta.setRuedas(ruedas.toLowerCase());

	}

	@Override
	public void buildMango() {
		bicicleta.setMango("cuero");

	}

	@Override
	public void buildManubrio() {
		bicicleta.setManubrio("aluminio");

	}

	@Override
	public void buildcadena() {
		bicicleta.setCadena("2,2 metros de largo");
	}

	@Override
	public void buildpi�on() {
		bicicleta.setPi�on("fijo");
	}

	@Override
	public void buildfrenos() {
		bicicleta.setFrenos("no tiene");
	}

}
