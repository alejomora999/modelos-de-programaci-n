package Ejercicio1;

public class CarreteraBuilder extends BicicletaBuilder {

	@Override
	public void buildMarco(String marco) {
		bicicleta.setMarco(marco.toLowerCase());

	}

	@Override
	public void builRuedas(String ruedas) {
		bicicleta.setRuedas(ruedas.toLowerCase());

	}

	@Override
	public void buildMango() {
		bicicleta.setMango("plastico");

	}

	@Override
	public void buildManubrio() {
		bicicleta.setManubrio("fibra de carbono");

	}

	@Override
	public void buildcadena() {
		bicicleta.setCadena("2 metros de largo");
	}

	@Override
	public void buildpi�on() {
		bicicleta.setPi�on("multiple");
	}

	@Override
	public void buildfrenos() {
		bicicleta.setFrenos("disco");
	}
	


}
