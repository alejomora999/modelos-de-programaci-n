package Ejercicio1;

public class Ensamblaje {
	private BicicletaBuilder bicicletaBuilder;
	private String marco="";
	private String ruedas="";
	public void montarBicicleta() {
		bicicletaBuilder.crearBicicleta();
		bicicletaBuilder.buildcadena();
		bicicletaBuilder.buildfrenos();
		bicicletaBuilder.buildMango();
		bicicletaBuilder.buildManubrio();
		bicicletaBuilder.buildMarco(getMarco());
		bicicletaBuilder.builRuedas(getRuedas());
		bicicletaBuilder.buildpi�on();
	
	}
	public void setBicicletaBuilder(BicicletaBuilder bici) {
		bicicletaBuilder=bici;
		
	}
	public Bicicleta getBicileta() {
		return bicicletaBuilder.getBicicleta();
	}
	public String getMarco() {
		return marco;
	}
	public void setMarco(String marco) {
		this.marco = marco;
	}
	public String getRuedas() {
		return ruedas;
	}
	public void setRuedas(String ruedas) {
		this.ruedas = ruedas;
	}

}
