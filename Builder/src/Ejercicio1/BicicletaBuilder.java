package Ejercicio1;

public abstract class BicicletaBuilder {
	protected Bicicleta bicicleta;
	public Bicicleta getBicicleta() {
		return bicicleta;
	}
	public void crearBicicleta() {
		bicicleta= new Bicicleta();
	}
	public abstract void buildMarco(String marco);
	public abstract void builRuedas(String ruedas);
	public abstract void buildMango();
	public abstract void buildManubrio();
	public abstract void buildcadena();
	public abstract void buildpi�on();
	public abstract void buildfrenos();

}
